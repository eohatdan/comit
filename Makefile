# Makefile for the COMIT project
#
# The project logo was made with https://www.myfonts.com/fonts/typodermic/minicomputer/bold/?refby=whatfontis


VERS=$(shell sed <comit -n -e '/version\s*=\s*"\(.*\)"/s//\1/p')

SOURCES = README COPYING NEWS TODO Makefile comit control *.adoc tests images comit-logo.png

all: comit comit.1 comit.html comit-paper.html comit-manual.html

clean:
	rm -f *~ *.1 *.html docs/*.html test/*~

.SUFFIXES: .html .adoc .1

# Requires asciidoc and a2x
.adoc.1:
	a2x --doctype manpage --format manpage $<
.adoc.html:
	asciidoc $<

check:
	cd tests; make --quiet

PYLINTOPTS = --rcfile=/dev/null --reports=n \
	--msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" \
	--dummy-variables-rgx='^_'
SUPPRESSIONS = --disable="C0103,C0111,C0200,C0301,C0410,C1001,R0903,R0902,R0911,R0912,R0914,R0915,W0511,W0611"
pylint:
	@pylint $(PYLINTOPTS) $(SUPPRESSIONS) comit

version:
	@echo $(VERS)

comit-$(VERS).tar.gz: $(SOURCES) comit.1
	tar --transform='s:^:comit-$(VERS)/:' --show-transformed-names -cvzf comit-$(VERS).tar.gz $(SOURCES) comit.1

dist: comit-$(VERS).tar.gz

release: comit-$(VERS).tar.gz comit.html comit-paper.html comit-manual.html
	shipper version=$(VERS) | sh -e -x

refresh: comit.html
	shipper -N -w version=$(VERS) | sh -e -x
